FROM ubuntu:latest

# Install tmate and any other dependencies
RUN apt-get update && apt-get install -y tmate

# Copy the custom tmate<  configuration file into the Docker image
COPY custom_tmate.conf /path/to/custom_tmate.conf

# Set the custom configuration file for tmate using the -F option
CMD ["tmate", "-F", "/path/to/custom_tmate.conf"]
